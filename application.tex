In the last section, we demonstrate the performance of RSVD on an application called robust principal component analysis (RPCA)~\cite{candes2011robust} summarized in Algorithm~\ref{alg:rpca}, which intends to demonstrate the possibility of accelerating time consuming machine learning applications on GPUs. To our knowledge, this work is the first to implement RPCA on GPUs.

RPCA is a common tool in computer vision and machine learning, which recovers a low-rank matrix with an unknown fraction of data corruption~\cite{candes2011robust}. While effective, it is known that RPCA is computationally demanding due to that the currently known algorithms require solving SVD multiple times. We now demonstrate the acceleration of RPCA using RSVD by taking the advantage of GPUs.
%While applications like neural networks benefit a lot by accelerating on GPUs, statistical learning algorithms like RPCA do not benefit much from this emerging parallel computing approach. Mainly this is caused by the low flop/s of SVD on GPUs, which hinders adopting GPUs to accelerate RPCA.
RPCA separates the sparse corruptions $\mathbf{S}$ from the original data $\mathbf{M}$ so that a low-rank matrix $\mathbf{L}$ can be obtained as $\mathbf{L} = \mathbf{M} - \mathbf{S}$. The problem can be written as:
\begin{equation}
\label{eqn:06}
\min_{\mathbf{L}, \mathbf{S}}{||\mathbf{L}||_{*}+\lambda||\mathbf{S}||_{1}}  \ \ \ \mathrm{subject} \ \mathrm{to}  \ \ \ \mathbf{M} = \mathbf{L} + \mathbf{S}, 
\end{equation}
where $||\cdot||_{*}$ and $||\cdot||_1$ denote the nuclear norm and $\ell_1$ norm of a matrix, respectively, and $\lambda$ is a positive weighting parameter that is usually set to $1/\sqrt{\max(m,n)}$. Various solvers have been proposed for this convex optimization problem, and recent solution methods drastically improved the efficiency~\cite{yuan2009sparse, lin2009fast, lin2010augmented}.
%However, although new solvers take fewer iterations to converge, they are limited to
%process small size matrix due to the high computation cost of Singular Value Decomposition (SVD)
%in every iteration. Therefore, it is different for a single node computer to handle a matrices 
%with dimensions larger than $m = 10^6$, which is a normal size in machine learning. 
%This limits the practical usage especially applications requires real-time processing.

%To address this problem, a lot of approaches have been proposed to accelerate the SVD,
%randomized SVD, partial SVD and etc.


%Robost principal component analysis (RPCA) is 
%For different solvers of PCA, performance comparsion regarding to speed
% and accuary is necesary. Andrews \textit{et al.} made a comprehensive
%library called LRSLibrary \citet{lrslibrary2015} which includes a lot of solvers proposed in last few years.
 In a similar manner proposed by Oh~\etal~\cite{oh2015fast}, we have used the out-of-GPU-memory RSVD for the SVD solver that appears in the inner loop of the RPCA. The shrinkage operator ${\mathcal{S}}_{{\varepsilon}}$ in line 4 of Algorithm~\ref{alg:rpca} is defined as
\begin{equation}
{\mathcal{S}}_{\varepsilon}=\left\{
\begin{aligned}
x-{\varepsilon}, & \quad \textup{if} \quad x>{\varepsilon}, \\ 
x+{\varepsilon}, & \quad \textup{if} \quad x<{-\varepsilon}, \\ 
0, & \quad \textup{otherwise.}\\
\end{aligned}
\right.
\end{equation}

\begin{spacing}{1}
    \begin{algorithm}[tb]
        \caption{RPCA by RSVD}
        \label{alg:rpca}
        \SetKwInOut{Input}{Input}
        \SetKwInOut{Output}{Output}
        % \Indm
        \Input{ matrix $\mathbf{M} \in \mathbb{R}^{m \times n}$, target rank $k$, oversampling parameter $o$, power iteration exponent $q$, convergence condition $\mathrm{tol}$.}
        \Output{ low-rank matrix $\mathbf{L}\in \mathbb{R}^{m \times n}$, and sparse matrix $\mathbf{S}\in \mathbb{R}^{m \times n}$}
        % \Indp\Indpp
        \BlankLine
        $\mathbf{Y}_0 = \mathbf{M} \ / \ \max(||\mathbf{M}||_2, \ \lambda^{-1}||\mathbf{M}||_{\infty});\  \mathbf{S}_0 = \mathbf{0}; \mu_{0} > 0;\  \rho > 1;\  i = 0$
        
        \While{ TRUE~ }{
            $[\mathbf{U}, \mathbf{\Sigma}, \mathbf{V}^T] = \mathrm{rsvd}(\mathbf{M} - \mathbf{S}_{i} + {\mu_i}^{-1} \mathbf{Y}_i \ ,\ k, \ o, \ q)$;
            
            $\mathbf{L}_{i+1} = \mathbf{U}{\mathcal{S}}_{{\mu_i}^{-1}}[\mathbf{\Sigma}] \mathbf{V}^T$\tcp*{shrinkage operation on $\mathbf{\Sigma}$}
            
            $\mathbf{S}_{i+1} = \mathcal{S}_{{\lambda\mu_i}^{-1}}[\mathbf{M} - \mathbf{L}_{i+1} + {\mu_i}^{-1}\mathbf{Y}_{i}]$;
            

            
            $\mathbf{Z}_{i+1} = \mathbf{M} - \mathbf{L}_{i+1} - \mathbf{S}_{i+1}$;
            
            $\mathbf{Y}_{i+1} = \mathbf{Y}_{i} + {\mu}_i\mathbf{Z}_{i+1}$;
            
            \textbf{if}$\ (\ ||\mathbf{Z}_{i+1}||_F \ / \ ||\mathbf{M}||_F < \mathrm{tol}\ ) \ $ \textbf{break}\tcp*{evaluate convergence}
            
            $\mu_{i+1}=\rho\mu_i; \ \ i= i+1$;  
        }
        
    \end{algorithm}
\end{spacing}

\begin{figure}[tb]
    \centering
    \includegraphics[width=1\hsize]{figs/RPCA.pdf}
    \caption{Sample outputs of RPCA.}
    \label{fig:background}
\end{figure}

A high definition traffic video is used in the experiment, which is heavily corrupted by camera jittering and large traffic volume. The detailed setup is included in \Tref{fig:background}. The sample frame outputs are shown in \fref{fig:background}, which indicates that our implementation successfully separated the input data into ``sparse'' traffic noise and the ``low-rank'' background. Regarding performance, our method accelerated $10.9\times$ for the RSVD computation which almost halved the overall run time by a multi-CPU implementation.

\begin{table}[tb]

    \centering
    \caption{Comparison of RPCA by RSVD on CPU and GPU. All experiments are
        conducted in double precision. The hardware setup is the same in \sref{sec:test}. The sampling parameters for RSVD are set as: $k=o=200$, and $q=4$. The parameters in Algorithm \ref{alg:rpca} are set as : $tol=10^{-6}, \rho=1.5$ and $\mu = 1.5/||\mathbf{M}||_{\infty}$.}
    \label{tb:rpca}
   % \begin{adjustbox}{width=1\textwidth}
        \begin{tabular}{cccc|ccccc}
            
            Data &Image \#&Size (GB) & $\mathrm{tol}$& & Iterations & RSVD time (s) & Total time (s)\\
            \hline
            Outdoor traffic & 1000 & 16 &$10^{-6}$ &CPU  & 28 & 1113 & 2030\\
            (1920$\times$1080)& & & &GPU & 28 & 102 & 1018\\
            \hline
        \end{tabular}
    %\end{adjustbox}
\end{table}
