In this section, we first give a straightforward out-of-GPU-memory implementation and compare with multi-CPU and cublas-XT implementations. Then, we show the performance of the proposed method against the basic implementation. Since the proposed method is essentially based on 1D GEMM, we dedicate a subsection to performance tuning and comparison of our 1D GEMM with library routines. In the last subsection, we use real data to validate the accuracy of our proposed method.

\input{setups}
% ----------------------------------------------------------------- %
\subsection{Baseline Implementation}
% =============================================================== %
\begin{spacing}{0.8}
	\begin{algorithm}[t]
		\caption{Cholesky QR Factorization: \footnotesize
                         $\mathbf{R} := chol(\mathbf{B})$ returns the Cholesky factorization of $\mathbf{B}$}
		\label{alg:cholqr}
		\SetKwInOut{Input}{Input}
		\SetKwInOut{Output}{Output}
		%\Indm
		\Input{$\mathbf{\widehat{P}}$ distributed in 1D block cyclic among GPUs, 
                       $\mathbf{\widehat{P}}_{(J,:)}$ is the $J$-th block row, and $s$ is the number of block rows.}
		\Output{Orthonormal $\mathbf{P}$ and upper-triangular $\mathbf{R}$ such that $\mathbf{P}\mathbf{R} = \widehat{\mathbf{P}}$ }
                $\mathbf{B} := 0$ \tcp*{On each GPU} 

		\For {$j = 0, 1, \cdots, s-1$}{
			$\mathbf{B}  \mathrel{+}= \widehat{\mathbf{P}}_{(J,:)}^{\top} \widehat{\mathbf{P}}_{(J,:)}$
                        \tcp*{On corresponding GPU} 
		}
		$\mathbf{R} := chol(\mathbf{B})$ \tcp*{accumulated and factored on CPU} 
		\For {$j = 0, 1, \cdots, s-1$}{
			$\mathbf{P}_{(J,:)}  := \widehat{\mathbf{P}}_{(J,:)}\mathbf{R}^{-1}$
                        \tcp*{On corresponding GPU} 
		}
	\end{algorithm}
\end{spacing}
% =============================================================== %
We first implemented RSVD with 1D row block GEMM and refer to this straightforward implementation as ``Baseline''. The performance comparison against multi-CPU implementation by LAPACK and GPU implementation by cuBLAS-XT are shown in \fref{fig:naive}(a).
%We use the out-of-GPU-memory GEMM (either our implementation or cuBLAS-XT) 
%for our first ``Basic'' implementation of the RSVD algorithm shown in Algorithm~\ref{alg:rsvd}.
%Hence, the matrix $\mathbf{A}$ is moved to the GPUs in the 1D block row cyclic fashion.
%
%Since our focus is on the tall-skinny $\mathbf{A}$ (i.e., $\ell \ll n \ll m$),
%we assume that the small $n \times \ell$ projection matrix $\mathbf{Q}$ fits in a single GPU memory, and hence is duplicated on all the GPUs.
%We evenly distribute the larger $m \times \ell$ matrix $\mathbf{P}$ among the GPUs
%in the same 1D block row fashion as the matrix $\mathbf{A}$.
%

%Each GPU redundantly computes the QR  factorization of $\mathbf{Q}$ using the Household QR implemented by MAGMA.
%The calculation of Gram matrix $\mathbf{B}$ is ``fused'' with the calculation of $\mathbf{P}_{(J,:)}$ 
%(see Line $9$$\sim$$12$ in Algorithm~\ref{alg:single}). 
%Upper triangular matrix $\mathbf{\widetilde{B}}$ is computed by Cholesky QR~\cite{Stath:2002} on the CPU. 

%For cuBLAS-XT implementation, the tile size is set to be big enough so as to perform 1D partition on the data. Matrix $\mathbf{P}$ and $\mathbf{U}$ are allocated as GPU memory which is faster than set to CPU memory in our experiments.

First, we noticed that ``Baseline'' and ``cuBLAS-XT'' outperformed the multi-CPU implementation by a large margin. Note that the peak performance of multi-CPU is only one-tenth of a single V100 GPU. The performance of ``Baseline'' and ``cuBLAS-XT'' are close and almost overlapped in one GPU setup. We then use two GPUs to give a further comparison.
As shown in \fref{fig:naive}(b), the ``baseline'' scales well achieving the speedups of up to $1.96\times$ using two GPUs compared to one GPU. The performance of cuBLAS-XT degrades on two GPUs due to the excessive data transfer between two GPUs. The performance contrast derives from the different out-of-GPU-memory GEMM implementation strategy. We will give a dedicated analysis in \sref{sec:GEMM_EXP}.

% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.5\hsize]{figs/naive.pdf}
	\caption{(a) Performance comparison of CPU, ``cuBLAS-XT'' and ``Baseline'' implementations of RSVD The parameters are set to $n = 5000,\ \ell=500,\ q=4$. The ``Baseline" and ``cuBLAS-XT'' are almost overlapped in (a).}
	\label{fig:naive}
\end{figure}
% =============================================================== %

% ----------------------------------------------------------------- %
\subsection{Performance of Proposed Methods}
We applied the proposed methods to ``Baseline'' step by step to show the performance impact of each one. \Fref{fig:gram}(a) and (b) show that the ``FIFO'' scheme effectively reduced the overall runtime by about $10\%$.
By reducing the number of the data passes from $2q+1$ to $q+1$, ``Single'' improved the performance by a factor of about $2\times$. Finally, the run time was almost halved by explicitly forming the Gram matrix. \Fref{fig:gram}(b) also shows that all proposed schemes scale well on two GPUs.
% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.5\hsize]{figs/gram.pdf}
	\caption{RSVD performance with different implementations, and increasing number of rows in $\mathbf{A}$ ($\ell = \frac{n}{10}$ and $p=4$). }
	 %(c) and (d) are conducted in double precision instead of double complex in order to show ``wider'' matrices.
	\label{fig:gram}
\end{figure}
% =============================================================== %
We then pick out ``Single'' and ``Gram'' to experiment with different power iteration number $q$. We use two kinds of shapes: (a) extremely tall-skinny ($n=1000$) and (b) moderately tall-skinny ($n=5000$).
With iteration $q=1$, ``Single'' and ``Gram'' did not show much performance difference in \fref{fig:power}(a) and (d). ``Single'' performs even better due to the less flops in \fref{fig:power}(b). With higher iterations, the run time of ``Single'' increases linearly with~$q$ in both \fref{fig:power}(a) and (b). However, with the explicitly forming Gram matrix, data pass for ``Single'' does not change with $q$, and the increasing number of $q$ did not increase the overall run time. The ``Gram'' shows its advantage that the run time almost stand still with the increasing $q$.
Hence, Gram matrix could give a significant performance improvement,
especially when a large number of iterations is required.
Two GPUs gives similar performance for ``Single'' and ``Gram'' comparison, we omitted the results here for brevity.

% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.5\hsize]{figs/power.pdf}
	\caption{``Single'' on ``Gram'' scheme performance with increasing number of power iterations \mbox{($\ell = \frac{n}{10}$)}.}
	\label{fig:power}
\end{figure}
% =============================================================== %

%=============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.5\hsize]{figs/breakdown.pdf}
	\caption{Time breakdown on one GPU with $\ell =n/10$ and $p=4$. 
		GEMM includes the time of CPU-GPU data transfer and GEMM operations.
		Misc. is composed of initialization and random matrix generation.}
	\label{fig:breakdown}
\end{figure}

% =============================================================== %
\Fref{fig:breakdown} shows the time breakdown of out-of-GPU-memory RSVD by the ``Gram'' scheme. We experimented with two setups: (a) growing the height of input matrix $\mathbf{A}$ and, (b) transforming the shape of $\mathbf{A}$ from tall-skinny to square-like. In \fref{fig:breakdown}(a), with the increasing height of $\mathbf{A}$, the proportion of GEMM gradually increased and took up to $95\%$ of the total run time. With transforming $\mathbf{A}$ from tall-skinny to square-like in \fref{fig:breakdown}(b), 
the portion of SVD increases with the increasing width of $\mathbf{A}$.
GEMM still takes up about $75\%$ of the total time for a square-like matrix.