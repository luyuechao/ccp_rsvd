In this section, we first review the RSVD algorithm, then we analyze and reveal the  performance bottleneck based a modified roofline model for accelerating RSVD on GPUs. 

\subsection{RSVD Algorithm}
RSVD has been made popular by Halko~\etal~\cite{halko2011finding}'s work built upon the previous studies on randomized linear algebra~\cite{frieze2004fast,martinsson2006randomized,sarlos2006improved,liberty2007randomized,rokhlin2009randomized}. It has been shown that for many applications,
the randomized algorithm requires only a few accesses to the dataset and outperforms the classical deterministic algorithm, while maintaining the desirable accuracy of the approximation~\cite{halko2011finding,martinsson2006randomized, liberty2007randomized, martinsson2010normalized}.

As shown in Algorithm~\ref{alg:rsvd};
given a matrix $\mathbf{A} \in \mathbb{R}^{m \times n}$, the RSVD algorithm first generates the basis vectors~$\mathbf{Q}$ and 
$\mathbf{P}$ that approximate the range and domain of 
the matrix $\mathbf{A}$, respectively.
Power iteration scheme ~\cite{Golub:2012}
(Lines $2-7$, Algorithm~\ref{alg:rsvd}) is applied to improve the accuracy of the approximation.
To maintain the numerical stability, basis vectors of $\mathbf{P}$ and $\mathbf{Q}$ are orthogonalized during the iterations.

%
After the power iteration, QR factorization is performed on $\mathbf{P}$ ( Line 9)
such that the upper triangular matrix $\mathbf{B}$ is the projected matrix
of dimension $\ell \times \ell$
(i.e., $\mathbf{B} = \mathbf{P}^T \mathbf{A} \mathbf{Q}$).
In other words, when matrix $\mathbf{A}$ is low rank, \ie, \hbox{$\mathrm{rank}(\mathbf{A}) = k \ll \min(m,n)$}, a small matrix $\mathbf{B}$ can be created and an SVD of the small matrix $\mathbf{B}$ reveals the SVD of the original matrix $\mathbf{A}$.
The SVD of $\mathbf{B}$ is computed by the deterministic SVD solver. Finally, to generate the left and right singular vectors of $\mathbf{A}$, the truncated SVD of $\mathbf{B}$ is projected back onto $\mathbf{Q}$ and $\mathbf{P}$ (Line 11).
As the outputs, $\mathbf{\Sigma}$ is the diagonal matrix whose diagonal entries approximate the $k$ largest
singular values of $\mathbf{A}$, while $\mathbf{U}$ and $\mathbf{V}$ approximate the corresponding
left and right singular vectors, respectively.
%Note that the orthonormalization of domain $\mathbf{P}$ can be omitted for shorter runtime.

\begin{algorithm}[t]
        
        \SetKwInOut{Input}{Input}
        \SetKwInOut{Output}{Output}
        %%      \Indm
        \Input{matrix $\mathbf{A} \in \mathbb{R}^{m \times n}$, target rank $k$, oversampling parameter $o$, and power iteration count $q$}
        \Output{$\mathbf{U}$, $\mathbf{\Sigma}$, and $\mathbf{V}$ such that $\mathbf{A} \approx \mathbf{U} \mathbf{\Sigma} \mathbf{V^{\top}}$
                with $k \times k$ diagonal $\mathbf{\Sigma}$, and orthonormal column vectors $\mathbf{U}$ and~$\mathbf{V}$.}

        %%      \Indp\Indpp
        \BlankLine
        Generate a random matrix $\mathbf{Q} \sim \mathcal{N}\left(0,1\right)^{n \times \ell}$,
        where $\ell = k + o$.

        \For{$i = 0, 1, \cdots, q-1$}{
                $\mathbf{P} := \mathbf{A}\mathbf{Q};$

                $\mathbf{P} := orth\left(\mathbf{P}\right);$
                \ \/// if needed                                        

                $\mathbf{Q} := \mathbf{A}^{\top}\mathbf{P};$              

                $\mathbf{Q} := orth(\mathbf{Q});$                       
        }
        $\mathbf{P} := \mathbf{A}\mathbf{Q};$

        $\left[ \mathbf{P},\ \mathbf{B}\right] := qr\left(\mathbf{P}\right);$  

        $\left[\mathbf{\widetilde{U}},\ \mathbf{\widetilde{\Sigma}},\ \mathbf{\widetilde{V}}\right] := svd\left(\mathbf{B}\right);$

        $\mathbf{\Sigma} := \mathbf{\widetilde{\Sigma}}(1:k,1:k);$

        $\mathbf{U} := \mathbf{P} \mathbf{\widetilde{U}}\left(:, 1:k\right);$

        $\mathbf{V} := \mathbf{Q}\mathbf{\widetilde{V}}\left(:,1:k\right);$

        \caption{Randomized SVD:
        $orth(\mathbf{P})$ orthogonalizes the column vectors $\mathbf{P}$, 
        while $qr(\mathbf{P})$ and $svd(\mathbf{B})$ return the QR factorization and SVD of matrices $\mathbf{P}$ and $\mathbf{B}$, 
        respectively. We use $\mathbf{\widetilde{\Sigma}}(1:k,1:k)$ and $\mathbf{\widetilde{U}}\left(:, 1:k\right)$ to denote
        the leading $k \times k$ submatrix of $\mathbf{\widetilde{\Sigma}}$ and the submatrix consiting of the first $k$ columns
        of $\mathbf{\widetilde{U}}$, respectively.
        }
        \label{alg:rsvd}
\end{algorithm}

With $q$ times power iteration, $\mathbf{A}$ will be accessed $2q+2$ times. The RSVD algorithm is dominated by GEMM operations with access to input matrix $\mathbf{A}$. The GEMM performance will fluctuate drastically with different implementation strategies and different matrix shapes. As for this work, we mainly focus on tall-skinny matrices which is prevalent in machine learning and data science(\eg most datasets in the UCI machine learning repository~\cite{Dua:2017}). In the next subsection, we use a modified roofline model to reveal that the main computation of RSVD is bandwidth-bound which requires a data-access reducing strategy for out-of-GPU-memory implementation.

%It can be rephrased as that out-of-GPU-memory tall-skinny GEMM is band-width bound problem. 
%We will use roofline model to clarify this allegation in \Sref{sec:gemm}.

% ----------------------------------------------------------------- %
\subsection{Performance Analysis}
To reveal the impact of matrix shapes on the performance of out-of-GPU-memory RSVD, we propose a performance model based on the roofline model~\cite{williams2009roofline}.
The roofline model is widely adopted in analyzing linear algebra algorithms ~\cite{williams2007optimization}. It is the first performance model to give a visual insight into the bound and bottleneck of the performance of programs on certain hardware.
The roofline model is given by
\begin{equation}\label{eq:roofline}
T = \min\left( \frac{F}{D} \times B,\ P \right)
\end{equation}
where $T$ denotes attainable Tflop/s; $F$ denotes the floating-point operations; $D$ denotes the communication cost or transfered data;
$B$ denotes the hardware's peak bandwidth; and $P$ denotes the peak computational performance of the hardware.
Furthermore, $F/D$ is called operational intensity and we denote it as $O$. As $B$ and $P$ are constants for certain hardware, $O$ is the key factor to influence performance.To illustrate the impact of matrix shapes on the out-of-GPU-memory GEMM performance, we substitute the operational intensity with the actual variables in the GEMM computation. The flop $F$ of GEMM is fixed to $2mn\ell$ in any setups, while data transfer $D$ between CPU and GPU varies from different implementations.

We used two setups for performance analysis: (1) the ideal case where the data transfer required for the GEMM opearation is minimum, i.e., $mn+n\ell+m\ell$ and $mn$ for copying matrices forth and back, respectively. As the CPU-GPU communication is full-duplex, data transfer can be simplified as $D = mn+n\ell+m\ell$. Therefore, the attainable Tflop/s will be
\begin{equation}\label{eq:ideal}
T_1  =  \min\left( \frac{2mn\ell}{mn+n\ell+m\ell} \times B,\ P \right)
\end{equation}

(2) A basic 2D GEMM implementation with block size $b$. We suppose that matrix $\mathbf{Q}$ and $\mathbf{P}$ are comparatively small and fit in the GPU, while $\mathbf{A}$ is large which has to be transferred to the GPU for each block GEMM. Therefore, the data transfer will be $D = (2n/b+1)m\ell+n\ell$. The attainable Tflop/s can be calculated as: 

\begin{equation}\label{eq:2Dgemm}
T_2  =  \min\left( \frac{2mn\ell}{(2n/b+1)m\ell+n\ell} \times B,\ P \right)
\end{equation}
%$T$ is inversely proportional to the partition block size $b$.  

% observation
Both equations \ref{eq:ideal} and \ref{eq:2Dgemm} are functions of three variables: $m$, $n$ and $\ell$; while all other parameters are constant for a specific hardware. 
To visualize the performance upper-bound, we reduce the variable by setting $l = n$( matrix $\mathbf{Q}$ is square) and $n:l = 10:1$ ( matrix $\mathbf{Q}$ is tall-skinny) so that $T$ is a function of $m$ and $n$. Combined with two previous setups, we get four performance upper-bound in \fref{fig:roofline}. The upper-bounds are shown in two-dimensional heatmaps, where the vertical and horizontal axes are the matrix dimensions $n$ and $m$ of $\mathbf{A}$, respectively. The heatmaps illustrates the performance upper-bound in different colors.

The lower part of \Fref{fig:roofline}(a) shows that even for an ideal case; when $\mathbf{A}$ is tall-skinny ($m \gg n$), GEMM is bandwidth-bound. The bandwidth-bound area spreads when the ratio of $n:l$ increases to $10:1$ in \Fref{fig:roofline}(b). \Fref{fig:roofline}(c) and (d) shows identical results which means that $\ell$ does not influence performance. However, the excessive transfer for blocks saturates the narrow CPU-GPU bandwidth quickly.

We make the following observations:
(1) Implementing out-of-GPU-memory RSVD based on 2D GEMM scheme will be bandwidth-bound, due to the 2D partition which inevitably increases data transfers. Performance will deteriorate especially for the tall-skinny GEMM which is the main focus of our research.
(2) 1D GEMM is a possible solution for tall-skinny matrices which can be implemented with the minimum amount of data transfers. In the next section, we propose a scheme to reduce the amount of accessed data based on 1D GEMM.

% =============================================================== %
\begin{figure}[tb]
	\centering
	\includegraphics[width=1\hsize]{figs/roofline.pdf}
	\caption{Performance upper-bound of out-of-GPU-memory GEMM: We use the parameters of a typical workstation platform with PCIe 3.0 link to the GPU. The peak bandwidth $B$ is set as
		$13$ GB/s, $7$ Tflop/s as the theoretical peak performance $P$ of Nvidia Volta GPU. Block size $b$ for (c) and (b) is set to $1024$, which is the default setup of cuBLAS-XT.
	}
	\label{fig:roofline}
\end{figure}

~\Fref{fig:roofline}(a) and (b) illustrates the performance upper-bound ($n=\ell$).  with the tall-skinny $\mathbf{A}$ (i.e., $m \gg n$), the GEMM performance is communication-bound.
In addition, with the $n \times \ell$ matrix~$\mathbf{Q}$, 
the operational intensity is $\ell$ (i.e., performing $\ell$ flops for each matrix entry of $\mathbf{A}$ copied to the GPU).
Hence, the performance becomes bounded more by communication 
as the number of columns in $\mathbf{Q}$ decreases 
(i.e., a smaller value of $\ell$, and hence skinner $\mathbf{Q}$).
On the other hand, as the value of $\ell$ increases,
the GEMM performance may attain the computation peak performance 
when the data transfer is carefully implemented. 
%\fref{fig:roofline}(b), (d) shows that a 2D tiling strategy will degrade the performance ceilings to all bandwidth-bound 
%for out-of-GPU-memory GEMM. It illustrates that an efficient tiling and computation strategy are critical to performance.


%We use a data reuse rate $r$ to denotes the reuse time of a block from $\mathbf{A}$ or $\mathbf{B}$.
%Our system is equipped with two Nvidia P100 GPU which is linked to two CPUs by two PCIe 3.0 links. 



