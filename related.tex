
Randomized algorithms~\cite{frieze2004fast,martinsson2006randomized,sarlos2006improved,liberty2007randomized,rokhlin2009randomized,mahoney2011randomized, halko2011finding} have been proposed to reduce data dimensions in solving problems with high-dimensional data. The efficiency of such algorithms in tackling large scale data has resulted in an increased interest from the high-performance computing (HPC) community.

\subsection{Randomized Algorithms}
Techniques called \emph{sampling} or \emph{sketching} perform operations only on 
a selected subset of the original data to reduce the cost of computation, communication and storage. Especially in the field of \textit{big data} where large quantity of data are missing or ``polluted'' by noise, only low-precision approximations are required. The dimensionality reduction is based on sampling matrix, also called sketching matrix. Sampling methods can be roughly categorized into 2 classes by the types of sketching matrices: uniform and non-uniform. 

 \textbf{Uniform sampling} uses i.i.d. standard Gaussian random number as the entries of sketching matrices and sample the matrices by matrix multiplication. This method is also called random projection. It has a strong relative-error bound and widely used in randomized matrix factorization. On the other hand, it has comparatively higher computational cost than non-uniform sampling. Particularly, given a matrix $\mathbf{A} \in \mathbb{R}^{m \times n}$ and sketching matrix $\mathbf{\Omega} \in \mathbb{R}^{n \times l}$, computing its sampled matrix $\mathbf{A\Omega}$ has the complexity of $\mathcal{O}(mnl)$.
 
 \textbf{Non-uniform sampling} constructs the subspace by selecting a certain set of vectors from the input data. First an ``importance sampling distribution'' of the input matrices is computed and the selecting is performed based on it~\cite{drineas2006fast}. The non-uniform sampling is proved to be more efficient than uniform sampling for applications like Gram matrix approximation~\cite{holodnak2015randomized} and second-order convex optimization~\cite{xu2016sub}.

% randomized algorithm in linear algebra
In the area of matrix factorization or approximation, randomized methods project the high-dimensional data into an low-dimensional subspace by exploiting the low-rank characteristics of the data. Then, a deterministic decomposition is performed on this ``sketch'', and projected back to form the full factorization.
% randomized alogirhtm in numerical optimization
In the field of numerical optimization and machine leaning, a great deal of efforts have been made in adopting randomized methods for large-scale optimization problems. Stochastic gradient descent (SGD)~\cite{robbins1985stochastic} and its variants ~\cite{duchi2011adaptive,kingma2014adam} are popular in first-order approximation of differentiable objective function. A series of sampled Newton methods~\cite{byrd2011use,roosta2016sub,pilanci2017newton} have been proposed to reduce the Hessian matrix computation and storage cost in second-order approximation and proved to be efficient on GPU accelerated systems~\cite{kylasa2018gpu}.

%Though these algorithms may dramatically reduce the data access,it depends on the data setshow much data needs to be sampled for maintaining the quality of the approximation, and if the sampled data can fit in a core memory.
  %The randomization process is mainly composed of tall-skinny GEMMs which is our main focus here.
%In contrast, we perform the randomized algorithm on the whole data set.
%This improves the robustness of the algorithm.
%These sampling algorithms may be combined with our implementation 
%to reduce the amount of data access and improve the performance.

%% background of RSVD
%RSVD was summarized and extended by Halko~\etal~\cite{halko2011finding} on top of the works of ~\cite{martinsson2006randomized, liberty2007randomized}, their approach outperformed classical deterministic methods as to speed, while maintained equivalent accuracy and robustness.

%Algorithm \ref{alg:rsvd} illustrates that most computations of RSVD are GEMMs. The experiments in \cite{mary2015performance} also show that $75\%$ time of RSVD is spent on GEMMs. For decomposing large data on GPUs, the out-of-GPU memory GEMM is the key influencing factor to performance.

\ignore{
	%% Parallel GEMM 
	To our knowledge, Cannon's Algorithm\cite{cannon1969cellular} is the first parallel GEMM algorithm. It lays out the matrices $\mathbf{A}$, $\mathbf{B}$ and $\mathbf{C}$ across a $p^{1/2}$-by-$p^{1/2}$ grid of total $p$ processors. The data block of processor will move between adjacent processors horizontally for $\mathbf{A}$ and vertically for $\mathbf{B}$ like a slot machine. The data block of $\mathbf{C}$ will be updated in each step. It has the weakness that it is hard to implement with (1) p processors not a square, (2) rectangular matrices.
	Also, In order to overlap communication and computation, the buffer for holding $\mathbf{A}$ and $\mathbf{B}$ on each processor will be doubled, which will limit the maximum data size one system can process.
	SUMMA\cite{van1997summa} refines Cannon's Algorithm by replacing data movement by broadcasting smaller blocks through processors so that computation and communication can be overlapped without introducing excessive memory burden.
	%3D GEMM  arrange processors in a cube with $p^{1/3}$ edge length. 
	More recent researches like 3D GEMM and 2.5D GEMM\cite{solomonik2011communication} have different memory-communication trade-offs in dealing distributed GEMM. 
	In these algorithms, the communication cost for initial distribution is not taken into consideration.\\
	
	%Efforts have been made to optimize GEMM to attain highest performance\cite{volkov2008benchmarking, agullo2009numerical}.
	%%
}


%　Efforts have been made to optimize GEMM to attain highest performance\cite{volkov2008benchmarking, agullo2009numerical}.

\ignore{
	\paragraph{Other Applications}
	Besides RSVD, there are a few other applications that require a tall-skinny out-of-GPU-memory GEMM. 
	
	%Sparse eigenvalue solver~\cite{kreutzer2016performance} uses GPU to accelerate tall-skinny GEMM in its subspace orthogonalization step.
}

\subsection{High-performance Computing for Randomized Algorithms}
The growing cost of computation and communication for processing large-scale data have aroused interest in randomized algorithms in the high-performance computing community. For accelerating the randomized algorithms with GPU accelerator, its performance has been studied in computing a low-rank approximation of a dense matrix based on a truncated SVD~\cite{ji2014gpu} and a truncated QR factorization with column pivoting \cite{mary2015performance}.
RSVDPACK is a library that contains a set of randomized algorithms for computing a low-rank approximation on a single GPU~\cite{voronin2015rsvdpack}. All these works assume that the data set is small enough to fit in the GPU memory at once, which limits the maximum data size.
Utilizing GPU to compute traditional matrix factorization algorithms has been going on for more than one decade~\cite{volkov2008benchmarking, agullo2009numerical}. Recently, algorithms have been extended to perform computation with data size exceeds the GPU memory capacity, including LU, QR, or Cholesky factorization ~\cite{d2012parallel,yamazaki2012one, yamazaki2016non}. A multi-GPU package callled BLASX\cite{wang2016blasx} adopts a 2-level hierarchical cache and a least-recently-used cache management for the efficient data access and reuse.
%We will compare the performance of our implementation with that of BLASX.
%Unfortunately, neither BLASX nor cuBLAS-XT is fully optimized for the particular shapes of the matrices of our interests.
Although GPU accelerators are prevalent in the HPC community, GPU programming is still difficult due to its complex memory hierarchy levels. To free the programmer from manually managing the CPU and GPU memory to gain high performance, NVIDIA introduced Unified Memory Access~(UMA) to simplify the CPU-GPU memory management by using the same memory pointer. Hence, GPU programs can be executed without manually managing data movement between CPU and GPU. This also makes it possible to execute GPU kernels on data that does not fit in the GPU memory at once.

\ignore{
% communication cost
On the modern computers, the cost of accessing data has become significantly more expensive compared with the cost of performing the arithmetic operations.
Driven by this hardware trend, researchers have been redesigning the algorithms to reduce the communication cost~\cite{ballard2011minimizing, demmel2013communication, anderson2011communication,haidar2017high}.
}