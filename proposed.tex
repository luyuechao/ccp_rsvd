% ----------------------------------------------------------------- %
In this section, we elaborate our schemes to reduce out-of-core data access. As concluded in the previous section, the bandwidth-bound is the performance bottleneck of out-of-GPU-memory GEMM. Therefore, reducing data access between CPU and GPU is pivotal to maximizing the performance. Our solutions are: (1) a FIFO buffer and single-pass per iteration based on 1D GEMM; (2) explicitly forming the Gram matrix to reduce data pass.
%
We summarize the computation and communication costs of different schemes in \Tref{tb:flops} before going to the details. Only the cost of power method is given in the table, which is the main difference of the proposed schemes.
%All the other part including QR and SVD has the same $\#flops$ and $\#word$. 
%The actual cost can be calculated as pass number multiply matrix size. All the following experiments will be performed in double complex as default.

% =============================================================== %
\begin{table}[tb]
	%	\small
	\centering
	\begin{tabular}{l|l|l} %% 6 column "|" means vertical line
		& \#flops                   & \#pass   \\
		\hline%% \\ take to the next column 
		Basic                     & $2(2q+1)mn\ell$           & $2q+1$ \\
		FIFO                      & $2(2q+1)mn\ell$           & $<2q+1$ \\ 
		Single-pass per iteration & $2(2q+1)mn\ell$           & $<q+1$ \\
		Explicit Gram             & $2(mn^2+qn^2\ell+mn\ell)$ & $\approx1.\times <2$  \\
	\end{tabular}
	\captionof{table}{\label{tb:flops}
		Computational and communication costs of different implementations:
		\#flops is the number of required flops in double precision while
		\#pass is the number of times $\mathbf{A}$ is accessed.}
\end{table}
% =============================================================== %

\subsection{Reducing out-of-core data access based on 1D GEMM}
% =============================================================== %
\begin{spacing}{0.8}
	\begin{algorithm}[t]
		\caption{FIFO buffer for power iteration}
		\label{alg:fifo}
		
		\For{$i = 0, 1, \cdots, q-1$}{
			\For {$j = 0, 1, \cdots, s-1$}{
				$\mathbf{P}_{(J,:)}  := \mathbf{A}_{(J,:)}\mathbf{Q};$
			}
			
			\For {$j = s-1, s-2, \cdots, 0$}{
				$\mathbf{Q} \mathrel{+}= \mathbf{A}_{(J,:)}^{\top}\mathbf{P}_{(J,:)}$	\tcp*{reuse 1D block}
			}
			$[\mathbf{Q},\ \sim ] := qr(\mathbf{Q});$
		}
		\For {$j = 0, 1, \cdots, s-1$}{
			$\mathbf{P}_{(J,:)} := \mathbf{A}_{(J,:)}\mathbf{Q};$
			
			$\mathbf{B}  \mathrel{+}= \mathbf{P}_{(J,:)}^{\top}\mathbf{P}_{(J,:)};$		
		}
		
	\end{algorithm}
\end{spacing}
% =============================================================== %
A straightforward implementation of RSVD passes $\mathbf{A}$ twice per iteration in the power method (Line 3--4, Algorithm~\ref{alg:rsvd}). We call it ``Baseline'' in the following content. Noticing that the main cost is accessing the large matrix $\mathbf{A}$, we first propose a ``FIFO'' buffer scheme based on 1D GEMM in Algorithm~\ref{alg:fifo}.
For calculating $\mathbf{Q}$, the accessing sequence of $\mathbf{A}$ (Line 6, Algorithm~\ref{alg:fifo}) is reversed so that 1D block of $\mathbf{A}^{\top}$ can be reused between each iteration.

%In the iteration loop of Algorithm \ref{alg:single}, the block index $j$ will go back and forth so as to reuse block in the buffer between each iteration. 
%The calculation of Gram matrix $\mathbf{B}$ is ``fused'' with the calculation of $\mathbf{P}_{(J,:)}$ 
%(see Line $9$$\sim$$12$ in Algorithm~\ref{alg:single}). 

%Besides moving the matrix $\mathbf{A}$ from the CPU to the GPU,
%all of our RSVD implementations transfer only the two small compressed matrices
%$\widetilde{\mathbf{Q}}$ and $\mathbf{B}$
%with the dimensions $n \times \ell$ and $\ell \times \ell$, respectively.

% ----------------------------------------------------------------- %
%\subsection{Power Iteration with Single-pass per Iteration}
To further reduce the data access, we propose a scheme called ``Single'' by taking advantage of the fact that the orthogonalization of $\mathbf{P}$ can be omitted in many applications. These orthogonalizations are used to ensure that
the different columns of $\mathbf{P}$ or $\mathbf{Q}$ do not
converge to the same dominant singular vector. For most data, only the orthogonalization of $\mathbf{Q}$ is needed. We will use synthetic and real data to validate the accuracy in ~\sref{sec:accuracy}.

In the ``Single'' scheme; for each row block $\mathbf{A}_{(J,:)}$,
we perform GEMM with both $\mathbf{A}_{(J,:)}$ and $\mathbf{A}_{(J,:)}^{\top}$
before accessing the next block.
Algorithm~\ref{alg:single} shows that
the block $\mathbf{A}_{(J,:)}$ is reused (Lines~4), so that 
the matrix $\mathbf{A}$ is accessed only once 
in every iteration. The cost of ``Single'' scheme
is the extra buffer $\widetilde{\mathbf{Q}}$, which has the same size as $\mathbf{Q}$. Since the size of $\mathbf{Q}$ is small, the extra storage cost is negligible.

% =============================================================== %
\begin{spacing}{0.8}
	\begin{algorithm}[t]
		\caption{Power method with single-pass per iteration}
		\label{alg:single}
		\BlankLine
		\For{$i = 0, 1, \cdots, q-1$}{
			\For {$j = 0, 1, \cdots, s-1$}{
				$\mathbf{P}_{(J,:)}  := \mathbf{A}_{(J,:)}\mathbf{Q};$
				
				$\mathbf{\widetilde{Q}} \mathrel{+}= \mathbf{A}_{(J,:)}^{\top}\mathbf{P}_{(J,:)}$	\tcp*{reuse 1D block}
			}
			$[\mathbf{Q},\ \sim ] := qr(\mathbf{\widetilde{Q}} );$
		}
	\end{algorithm}
\end{spacing}
% =============================================================== %

% ----------------------------------------------------------------- %
\subsection{Power Iteration with Forming Gram Matrix}
Some data may present a slow singular decay pattern which requires more iterations in power method to converge\cite{martinsson2010normalized, halko2011finding}. As $q$ increases, more passes to $\mathbf{A}$ will incur tremendous communication cost.
In most cases, forming the Gram matrix $\mathbf{G} = \mathbf{A}^{\top}$ is avoided due to high computation cost. In the GPU environment where computation cost is cheap compared to communication cost, we explicitly form the Gram matrix as $\mathbf{G} = \mathbf{A}^{\top}\mathbf{A}$, then perform the power iterations on $\mathbf{G}$. This ``Gram'' scheme is shown in Algorithm \ref{alg:gram}, which is mathematically equivalent to Algorithm \ref{alg:single}. Although extra storage and computation are required to form the Gram matrix (i.e., $\mathcal{O}(n^2)$ storage and $\mathcal{O}(mn^2)$ flops), it reduces data access of $\mathbf{A}$ to only one pass in power iterations.
With the large gap between communication cost and computation cost, forming $\mathbf{G}$ may reduce the overall time at the expenses of increased flops. We validate this assumption by experiments in the next section.

% =============================================================== %
\begin{algorithm}[t]
	\caption{Power method with explicit Gram matrix}
	\label{alg:gram}
	
	\BlankLine
	%Generate a random matrix $\mathbf{Q} \sim \mathcal{N}\left(0,1\right)^{n \times l}$,
	%where $l = k + o$.
	
	$\mathbf{G} = \mathbf{A}^{\top}\mathbf{A}$ \tcp*{Form Gram matrix by 1D GEMM}
	
	\For{$i = 0, 1, \cdots, q-1$}{
		$\mathbf{Q} := \mathbf{G}\mathbf{Q};$
		
		$\left[\mathbf{Q},\ \sim \right] := qr(\mathbf{Q});$
	}
	
\end{algorithm}
% =============================================================== %

%As the arithmetic and communication cost of sampling and power iteration are listed in \Tref{tb:flops}, we count the other part of the algorithm as $2(mnl+mlk+nkl)$ and $mn+nlq+ml+2l^2$ for arithmetic cost and communication cost respectively.

\subsection{Implementation Framework and Analysis}

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.5\hsize]{figs/partiton.pdf}
	\caption{1D block row cyclic partition of $\mathbf{A}$ for out-of-GPU-memory tall-skinny GEMM. (a) shows the $m>\!\!>k, m>\!\!>n$ situation, where $Q$ is scattered among multiple GPU. (b) shows the $k>\!\!>m, k>\!\!>n$ situation, where $Q$ is calculated by reduction.}
	\label{fig:1DGEMM}
\end{figure}

\Fref{fig:1DGEMM}(a) and (b) illustrates our partition scheme for two GEMM operations in the power iteration.  $\mathbf{P} = \mathbf{A}\mathbf{Q}$ (\fref{fig:1DGEMM}(a)),
is based on 1D row partition of tall-skinny matrix $\mathbf{A}$ and $\mathbf{P}$. 
The dimensions of each block is $b \times n$ and $b \times l$, where $b$ is the block size.
Since $\ell \ll m,n$,  the size of matrix $\mathbf{P}$ and $\mathbf{Q}$ is much smaller than $\mathbf{A}$. $\mathbf{P}$ and $\mathbf{Q}$ are placed in GPU memory,
while the input matrix $\mathbf{A}$ remains in CPU memory.
Transfer of blocks $\mathbf{A}$ is overlapped with the GEMM operation on GPU to maximize the hardware utilization. Blocks are assigned to the GPU streams in a round-robin fashion (i.e., 1D block row cyclic distribution).
%
The small $n \times \ell$ matrix $\mathbf{Q}$ 
is initialized on the CPU, and then broadcasted to all GPUs. The second GEMM $\mathbf{Q} = \mathbf{A}^{\top}\mathbf{P}$ is based on an outer-product of $\mathbf{Q}$. As shown in \fref{fig:1DGEMM}(b), $\mathbf{Q}$ is duplicated so that each GEMM operation is independent. A reduction of $\mathbf{Q}$ is followed after finishing all GEMM operations.
This framework has the following advantages:
(1)~Computation efficiency: If tall-skinny $\mathbf{A}$ is partitioned into 1D column blocks, the outer-product update of the matrix $\mathbf{P}$ results in a large amount of data transfer. Also, 1D column partition requires multiple buffers and complex synchronization mechanism to combine the updates from different GPU streams.

In contrast, the 1D block row partition,
each block GEMM operation is independent to each other. This property enables data transfer and GEMM operations of sequential blocks to be overlapped and executed in pipeline without synchronization.
(2) Higher GEMM performance: GEMM routines generally performs better on square matrices than tall-skinny ones. Compared with the 1D block column partition, 1D block row partition gives square-like blocks, which lead to higher overall performance.

For GPU implementation, GPU streams are used to overlap the transfer of a block
with the GEMM operation of another block on the GPU. The transfer and computation is dispatched in a round-robin fashion on multi-GPU. The QR factorization of $\mathbf{P}$, is computed by Cholesky QR~\cite{Stath:2002} (shown in \fref{alg:cholqr}) that only transfers the small $\ell \times \ell$ matrix $\mathbf{B}$ and $\mathbf{R}$ between CPU and GPU.
% ----------------------------------------------------------------- %