clc; clear; close all;
addpath('/Users/actionmask/googleDrive/software/altmany-export_fig-bb6c842');


print_png = 0;

spaceH=0;spaceV=0.14;marTop=0.04;marBot=0.1; 
padding=0.01;margin=0.1;marginL=0.1;

largeFont = 26;fsize = 18; LineWide = 2;
h1=figure; set(h1, 'position', [200, 200, 1050, 800]);

m=[1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,...
    20000,30000,40000,50000,60000,70000,80000,90000,100000,110000];

n=[1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,...
   13000,16000,19000,22000,25000,28000,31000,34000,37000,40000];

m_length = length(m);
n_length = length(n);
bandwidth = 0.013;  % CPU-GPU bandwidth in Tflops
apeak = 7;        % arithmetic peak

%%%%%%%%%%%%%%%%%%%%%%%%%%%(a)%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subaxis(2,2,1,'SpacingVert',spaceV,'SpacingHoriz', spaceH,...
        'PL',padding,'PR',padding,'mt',...  
        marTop,'mb',marBot,'ML',marginL,'MR',margin); 
Op2D = zeros(m_length, n_length); % operational intensity

% for test

% row major data!
for i = 1:m_length
    for j = 1:n_length
        Op2D(j,i) = (m(i).*n(j))...
            ./ ((2* m(i) + n(j))*4);
    end
end

roofline1 = min(Op2D * bandwidth, apeak);

pcolor(m, n, roofline1);

xlim([min(m) max(m)]);
ylim([min(n) max(n)]);
caxis([0 apeak]);

shading interp;
grid on; set(gca,'GridLineStyle',':');
set(gca,'layer','top')
set(gca,'Xscale', 'log','Yscale','log');
set(gca, 'fontsize', fsize);
xlabel({'$m$','(a) $l=n$, best-case'},...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('$n$', 'Interpreter', 'latex', 'fontsize', largeFont);
axis square;
%title('Roofline model for ', 'fontsize', fsize);
if print_png == 1
    set(gca,'LooseInset',get(gca,'TightInset'));
    set(gca, 'Visible', 'off');
else
    hcb =colorbar; 
    title(hcb,'Tflop/s', 'fontsize', fsize);
end

%%%%%%%%%%%%%%%%%%%%%%%(b)%%%%%%%%%%%%%%%%%%%%%%%%%%
subaxis(2,2,2,'SpacingVert',spaceV, 'SpacingHoriz', spaceH,...
        'PL',padding,'PR',padding,'mt',...  
        marTop,'mb',marBot,'ML',marginL,'MR',margin); 
Op2D = zeros(m_length, n_length); % operational intensity
roofline2 = zeros(m_length, n_length);


% for test
b = 1024;
% row major data
for i = 1:m_length
    for j = 1:n_length
        Op2D(j,i) = (2*m(i)*n(j)*n(j))...
            ./ ((2*m(i)*n(j)*n(j)./b + m(i)*n(j))*8);
    end
end

roofline2 = min(Op2D * bandwidth, apeak);
pcolor(m, n, roofline2);

xlim([min(m) max(m)]);
ylim([min(n) max(n)]);
caxis([0 apeak]);

colormap(jet(256)); 

shading interp;
grid on; set(gca,'GridLineStyle',':');
set(gca,'layer','top');
set(gca,'Xscale', 'log','Yscale','log');
set(gca, 'fontsize', fsize);
xlabel({'$m$','(b) $l=n,\ b=1024$, worst-case'},...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('$n$', 'Interpreter', 'latex', 'fontsize', largeFont);
axis square;
%title('Roofline model for 2D partition', 'fontsize', fsize);
if print_png == 1
    set(gca,'LooseInset',get(gca,'TightInset'));
    set(gca, 'Visible', 'off');
    
else
    hcb =colorbar; 
    title(hcb,'Tflop/s', 'fontsize', fsize);
end
%%%%%%%%%%%%%%%%%%%%%%(c)%%%%%%%%%%%%%%%%%%%%%%%%%
subaxis(2,2,3,'SpacingVert',spaceV, 'SpacingHoriz', spaceH,  ...
        'PL',padding,'PR',padding,'mt',...  
        marTop,'mb',marBot,'ML',marginL,'MR',margin); 
Op2D = zeros(m_length, n_length); % operational intensity

% for test
k = n*0.1;
% row major data!
for i = 1:m_length
    for j = 1:n_length
        Op2D(j,i) = (2*m(i).*n(j).*k(j))...
            ./ ((m(i).*k(j) + n(j).*k(j) + m(i).*n(j))*8);
    end
end

roofline1 = min(Op2D * bandwidth, apeak);

pcolor(m, n, roofline1);

xlim([min(m) max(m)]);
ylim([min(n) max(n)]);
caxis([0 apeak]);
colormap(jet(256)); 

shading interp;
grid on; set(gca,'GridLineStyle',':');
set(gca,'layer','top');
set(gca,'Xscale', 'log','Yscale','log');
set(gca, 'fontsize', fsize);
xlabel({'$m$', '(c)  $n:l = 10:1$, best-case'},...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('$n$', 'Interpreter', 'latex', 'fontsize', largeFont);
axis square;
%title('Roofline model for best case', 'fontsize', fsize);
if print_png == 1
    set(gca,'LooseInset',get(gca,'TightInset'));
    set(gca, 'Visible', 'off');
else
    hcb =colorbar; 
    title(hcb,'Tflop/s', 'fontsize', fsize);
end

%%%%%%%%%%%%%%%%%%%%%(d)%%%%%%%%%%%%%%%%%%%%%%%%%%%
subaxis(2,2,4,'SpacingVert',spaceV,'SpacingHoriz', spaceH,...
        'PL',padding,'PR',padding,'mt',...  
        marTop,'mb',marBot,'ML',marginL,'MR',margin); 
Op2D = zeros(m_length, n_length); % operational intensity
roofline2 = zeros(m_length, n_length);

% for test
b = 1024;
k = n*0.1;
% row major data
for i = 1:m_length
    for j = 1:n_length
        Op2D(j,i) = (2*m(i).*n(j).*k(j))...
            ./ (((2.*k(j)./b + 1).* m(i).*n(j))*8);
    end
end

roofline2 = min(Op2D * bandwidth, apeak);
pcolor(m, n, roofline2);

xlim([min(m) max(m)]);
ylim([min(n) max(n)]);
caxis([0 apeak]);
colormap(jet(256)); 

shading interp;
grid on; set(gca,'GridLineStyle',':');
set(gca,'layer','top');
set(gca,'Xscale', 'log','Yscale','log');
set(gca, 'fontsize', fsize);
xlabel({'$m$','(d) $n:l = 10:1,\ b=1024$, worst-case'},...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('$n$', 'Interpreter', 'latex', 'fontsize', largeFont);
axis square;
%title('Roofline model for 2D partition', 'fontsize', fsize);

if print_png == 1
    set(gca,'LooseInset', get(gca,'TightInset'));
    set(gca, 'Visible', 'off');
else
    hcb =colorbar; 
    title(hcb,'Tflop/s', 'fontsize', fsize);
end
if print_png == 1
    saveas(gcf,'junk.png');
else
    saveas(gcf,'junk.pdf');
end