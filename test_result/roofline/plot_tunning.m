clc;clear; close all;

fsize = 15; largeFont = 20; LineWide = 2;

test_num = 4;

marker = {'-d', ':', '-o', '--', '-x', '-*', '-^', '->', '-+', '-s', '-.'};

h1=figure; set(h1, 'position', [200, 200, 800, 400]);

bandwidth = 0.013; apeak = 6.0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%(a)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
block_data = csvread('block_n_1000.csv');
block_axis = block_data(1:13, 2);
block_data = reshape(block_data(:,6), [13, 11]);

subplot(1,2,1);
m = block_axis; n = 1000;
op1D2 = (m .* n) ./ (4*(2*m + n));
roofline1D = min(op1D2 * bandwidth, apeak);

plot(block_axis, roofline1D, '-', ...
     block_axis, block_data(:,1),'-d', ...
     block_axis, block_data(:,2),'-o', ...
     block_axis, block_data(:,4),'-x', ...
     block_axis, block_data(:,8),'-*', ...
     block_axis, block_data(:, 11),'-^', ...
     'LineWidth', LineWide);

xlim([min(m) max(m)]);
ylim([0 1.1* max(roofline1D)]);

h_legend = legend('roofline',...
       'block mb=1024',...
       'block mb=2048',...
       'block mb=4096',...
       'block mb=8192',...
       'maximize block size',...
       'Orientation','Vertical','Location','south');

set(h_legend,'FontSize',fsize); axis square;

set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(a) $n=1000, k = 1000$'},...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);
title('');
grid on; set(gca,'GridLineStyle',':');

%%%%%%%%%%%%%%%%%%% (b) %%%%%%%%%%%%%%%%%%%%%%%%
%%get the square data
block_data2 = csvread('block_n_5000.csv');
Xaxis2 = block_data2(1:11, 2);
max_axis2 = block_data2(122:134, 2);
max_size = block_data2(122:134, 6);
block_data2 = reshape(block_data2(1:121,6), [11, 11]);

m = Xaxis2; n = 5000;
op1D = (m .* n) ./ (4*(2*m + n));
roofline1D = min(op1D * bandwidth, apeak);
subplot(1,2,2);
plot(Xaxis2, roofline1D, '-', ...
     Xaxis2, block_data2(:,1),'-d', ...
     Xaxis2, block_data2(:,2),'-o', ...
     Xaxis2, block_data2(:,4),'-x', ...
     Xaxis2, block_data2(:,8),'-*', ...
     max_axis2, max_size,'-^', ...
     'LineWidth', LineWide);

xlim([min(Xaxis2) max(Xaxis2)]);
ylim([0 1.1*max(roofline1D)]);

%  h_legend = legend('roofline',...
%              'CublasXT',...
%              'BLASX',...
%             'block mb=1024', 'block mb=4096',...
%             'block mb=8192','maximize block size',...
%         'Orientation','Vertical','Location','north');
%  
%  set(h_legend,'FontSize',fsize); 
axis square;
set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(a) $n=5000, k = 5000$'},...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);

%title('n=5000, k = 5000');
grid on; set(gca,'GridLineStyle',':');
