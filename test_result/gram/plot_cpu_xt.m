clc; clear; close all;

fsize = 15; largeFont = 20; LineWide = 2;

test_num = 4;

marker = {'-d', ':', '-o', '--', '-x', '-*', '-^', '->', '-+', '-s', '-.'};

h1=figure; set(h1, 'position', [0, 0, 800, 400]);

bandwidth = 0.013; apeak = 7.0; %% 13GB/s for pci-e 12Tflops for 2GPU

%%%%%%%%%%%%%%%%%(a) 1gpu time %%%%%%%%%%%%%%%%%%%%%
subplot(1, 2, 1);
one_gpu = csvread('cpu_xt.csv');
m1= one_gpu(:, 1);
svd = one_gpu(:, 4);
cpu1 = one_gpu(:, 5);
basic1 = one_gpu(:, 6);
xt1 = one_gpu(:, 7);

%m1, cpu1, '->', ...
plot(m1, basic1, '-*', ...
     m1, xt1, '--^',...
     m1, cpu1, '-o', ...
     m1, svd, '-d', ...
 'LineWidth', LineWide);
xlim([min(m1) max(m1)]);

h_legend = legend('Naive(GPU)', 'cuBLAS-XT', 'two CPUs', 'SVD (CPU+GPU)',... 
       'Orientation','Vertical','Location','northwest');
   
% set(h_legend, 'FontSize', fsize);
axis square;

%set(gca,'Xscale', 'log'); set(gca,'Yscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(a) one GPU'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('time (s)', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

% %%%%%%%%%%%%%%%%% (b) 2GPUs %%%%%%%%%%%%%%%%%%%%%
subplot(1, 2, 2);

two_gpu = csvread('cpu_xt2.csv');
m2     = two_gpu(:, 1);
basic2 = two_gpu(:, 4);
xt2    = two_gpu(:, 5);

%m1, cpu1, '->', ...
plot(m2, basic2,  '-*', ...
     m2, xt2, '--^', ...
     'LineWidth', LineWide);
xlim([min(m2) max(m2)]);

h_legend = legend('Naive (GPU)','cuBLAS-XT',... 
       'Orientation','Vertical','Location','north');
   
% set(h_legend, 'FontSize', fsize);
axis square;
%set(gca,'Xscale', 'log');
%set(gca,'Yscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(b) two GPUs'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('time (s)', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');
