clc; clear; close all;

fsize = 15; largeFont = 20; LineWide = 2;

test_num = 4;

marker = {'-d', ':', '-o', '--', '-x', '-*', '-^', '->', '-+', '-s', '-.'};

h1=figure; set(h1, 'position', [0, 0, 800, 800]);

bandwidth = 0.013; apeak = 7.0; %% 13GB/s for pci-e 12Tflops for 2GPU
%%%%%%%%%%%%%%%%%(a) 1gpu time %%%%%%%%%%%%%%%%%%%%%
subplot(1, 2, 1);
one_gpu = csvread('gpu1_1000.csv');
m1= one_gpu(:, 1);
cpu1 = one_gpu(:, 4);
basic1 = one_gpu(:, 5);
fifo1 = one_gpu(:, 6);
single1 = one_gpu(:, 7);
gram1 = one_gpu(:, 8);
%m1, cpu1, '->', ...
plot(m1, basic1, '-o', ...
     m1, fifo1,  '-*', ...
     m1, single1, '-^', ...
     m1, gram1,   '-s',...
     'LineWidth', LineWide);
xlim([min(m1) max(m1)]);

h_legend = legend('Naive','FIFO','Single','Gram',... 
       'Orientation','Vertical','Location','northwest');
   
% set(h_legend, 'FontSize', fsize);
axis square;

%set(gca,'Xscale', 'log');
%set(gca,'Yscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(a) one GPU, $n=1000$'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('time (s)', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

% %%%%%%%%%%%%%%%%% (b) 2GPUs %%%%%%%%%%%%%%%%%%%%%
subplot(1, 2, 2);

two_gpu = csvread('gpu2_1000.csv');
m2= two_gpu(:, 1);
cpu2 = two_gpu(:, 4);
basic2 = two_gpu(:, 5);
fifo2 = two_gpu(:, 6);
single2 = two_gpu(:, 7);
gram2 = two_gpu(:, 8);
%m1, cpu1, '->', ...
plot(m2, basic2, '-o', ...
     m2, fifo2,  '-*', ...
     m2, single2, '-^', ...
     m2, gram2,   '-s',...
     'LineWidth', LineWide);
xlim([min(m1) max(m1)]);

h_legend = legend('Naive','FIFO','Single','Gram',... 
       'Orientation','Vertical','Location','northwest');
   
% set(h_legend, 'FontSize', fsize);
axis square;
%set(gca,'Xscale', 'log');
%set(gca,'Yscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(b) two GPUs, $n=1000$'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('time (s)', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');


% % %%%%%%%%%%%%%%%%% (c) 1 gpu cross %%%%%%%%%%%%%%%%%%%%%
% subplot(1, 2, 3);
% cross1 = csvread('cross_1gpu_n4e5.csv');
% n = cross1(:, 2);
% single1 = cross1(:, 7);
% gram1 = cross1(:, 8);
% 
% plot(n, single1, '-', ...
%      n, gram1, '-o', ...
%     'LineWidth', LineWide);
% 
% xlim([min(n) max(n)]);
% % %ylim([0 apeak]);
% set(gca, 'fontsize', fsize);
% h_legend = legend('Single', 'Gram',...
%        'Orientation','Vertical','Location','northwest');
% % set(h_legend, 'FontSize', fsize);
% axis square;
% %set(gca,'Xscale', 'log');
% %set(gca,'Yscale', 'log');
% set(gca, 'fontsize', fsize);
% xlabel({'$n$','(c) one GPU, $m = 4\times10^5$'}, ...
%        'Interpreter', 'latex', 'fontsize', largeFont);
% ylabel('time (s)', 'fontsize', largeFont);
% grid on; set(gca,'GridLineStyle',':');


% %%%%%%%%%%%%%%%%% (d) 2 gpu cross %%%%%%%%%%%%%%%%%%%%%
% subplot(2, 2, 4);
% cross1 = csvread('cross_2gpu_n4e5.csv');
% n = cross1(:, 2);
% single1 = cross1(:, 7);
% gram1 = cross1(:, 8);
% 
% plot(n, single1, '-', ...
%      n, gram1, '-o', ...
%     'LineWidth', LineWide);
% 
% xlim([min(n) max(n)]);
% % %ylim([0 apeak]);
% set(gca, 'fontsize', fsize);
% h_legend = legend('Single', 'Gram',...
%        'Orientation','Vertical','Location','northwest');
% % set(h_legend, 'FontSize', fsize);
% axis square;
% %set(gca,'Xscale', 'log');
% %set(gca,'Yscale', 'log');
% set(gca, 'fontsize', fsize);
% xlabel({'$n$','(d) two GPUs, $m = 4\times10^5$'}, ...
%        'Interpreter', 'latex', 'fontsize', largeFont);
% ylabel('time (s)', 'fontsize', largeFont);
% grid on; set(gca,'GridLineStyle',':');


% %%%%%%%%%%%%%%%%% (b) 1 GPU roofline model%%%%%%%%%%%%%%%%%%%%%
% m = one_gpu(:, 1);
% n = one_gpu(:, 2);
% k = one_gpu(:, 3);
% l = 2 .* k;
% subplot(2, 2, 2);
% %% (5e5* (5e5/50) * 16)/ 2^30 = 74.5GB for double complex
% %% naive power iteration flops
% q = 4;
% %% GEMM flops
% basic_flops = 8 * (2*q+1).*( m.*n.*l); % double complex
% gram_flops =  8 * (m.*(n.^2) + q * (n.^2).*l + m.*n.*l);
% 
% %% other flops
% o_flops = 8 * (m.*(l.^2)*q + m.*l.*k + n.*l.*k + l.^3);
% %% word
% basic_word = 16 * ((2*q+1) .* m .* n);
% gram_word = 16 * ( 2 * m .* n );
% o_word = 16 * (n.*l.*q + m.*l + 2*(l.^2));
% 
% basic_intense = (basic_flops + o_flops) ./ (basic_word + o_word);
% gram_intense  = (gram_flops  + o_flops) ./ (gram_word + o_word);
% basic_roof = min(basic_intense * bandwidth, apeak);
% gram_roof  = min(gram_intense * bandwidth, apeak);
% basic_total = (basic_flops + o_flops)./1e12; 
% gram_total  = (gram_flops  + o_flops)./1e12;
% 
% basic_flop = basic_total ./ basic1;
% gram_flop =  gram_total ./ gram1;
% %     
% plot(m, basic_roof, '-', ...
%      m, gram_roof, '-', ...
%      m, basic_flop, '-o', ...
%      m, gram_flop, '-^', ... 
%     'LineWidth', LineWide);
% xlim([min(m) max(m)]);
% %ylim([0 apeak]);
% 
% h_legend = legend('baisc roofline',...
%        'Gram roofline',...
%        'Basic',...
%        'Gram',...
%        'Orientation','Vertical','Location','northeast');
% 
% % set(h_legend, 'FontSize', fsize);
% axis square;
% 
% set(gca,'Xscale', 'log');
% set(gca, 'fontsize', fsize);
% %set(gca,'Xscale', 'log','Yscale','log');
% xlabel({'$m$'}, ...
%        'Interpreter', 'latex', 'fontsize', largeFont);
% ylabel('Tflop/s', 'fontsize', largeFont);
% grid on; set(gca,'GridLineStyle',':');
