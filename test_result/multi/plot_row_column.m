clc; clear; close all;

fsize = 15; largeFont = 20; LineWide = 2;

test_num = 4;

marker = {'-d', ':', '-o', '--', '-x', '-*', '-^', '->', '-+', '-s', '-.'};

h1=figure; set(h1, 'position', [0, 0, 800, 800]);

bandwidth = 0.013*2; apeak = 7.0 * 2; %% 13GB/s for pci-e 12Tflops for 2GPU
ts_data = csvread('multi_1D_perf.csv');
sw_data = csvread('multi_sw_perf.csv');
ts_col_data = csvread('ts_col_partition.csv');
sw_row_data = csvread('sw_row_partition.csv');

%%%%%%%%%%%%%%%%%(a) square 1000 * 1000 %%%%%%%%%%%%%%%%%%%%%
idx1 = 1:13;
Xaxis1 = ts_data(idx1, 2);
ts = ts_data(idx1, 8);
sw = sw_data(idx1, 8);
idx_col1 = 1:10;
axis_b1 = ts_col_data(idx_col1, 2);
ts_col = ts_col_data(idx_col1, 8);
sw_row = sw_row_data(idx_col1, 8);

subplot(2, 2, 1);
m = Xaxis1; n = 1000;
op1D1 = (m .* n) ./ (4*(2*m + n));
roofline1D = min(op1D1 * bandwidth, apeak);

plot(Xaxis1, roofline1D, '-', ...
     Xaxis1, ts, '-o',...
     Xaxis1, sw, '-*',...
     axis_b1, ts_col, '-^',...
     axis_b1, sw_row, '-x',...
     'LineWidth', LineWide);
xlim([min(m) max(m)]);
ylim([0 1.1* max(roofline1D)]);

% h_legend = legend('roofline',...
%        'Proposed',...
%        'cuBLAS-XT, pinned memory, 1D',...
%        'cuBLAS-XT, UMA, 1D',...
%        'cuBLAS-XT, pinned memory, 2D ',...
%        'BLASX',...
%        'Orientation','Vertical','Location','south');

% set(h_legend, 'FontSize', fsize);
axis square;

set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(a) $k = 1000,\ n = 1000$'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

%%%%%%%%%%%%%%%%%%% (b) square 5000*5000 %%%%%%%%%%%%%%%%%%%%%%%%
idx2 = 14:24;
Xaxis2 = ts_data(idx2, 2);
ts = ts_data(idx2, 8);
sw = sw_data(idx2, 8);
idx_col2 = 11:18;
axis_b2 = ts_col_data(idx_col2, 2);
ts_col2 = ts_col_data(idx_col2, 8);
sw_row2 = sw_row_data(idx_col2, 8);

subplot(2,2,2);
m = Xaxis2; n = 5000;
op1D2 = (m .* n) ./ (4*(2*m + n));
roofline1D = min(op1D2 * bandwidth, apeak);

plot(Xaxis2, roofline1D, '-', ...
     Xaxis2, ts, '-o',...
     Xaxis2, sw, '-*',...
     axis_b2, ts_col2, '-^',...
     axis_b2, sw_row2, '-x',...
     'LineWidth', LineWide);

xlim([min(Xaxis2) max(Xaxis2)]);


axis square;
set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(b) $k=5000,\ n = 5000$'}, 'Interpreter', 'latex',...
       'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);

grid on; set(gca,'GridLineStyle',':');


%%%%%%%%%% (c) rectangle 5000*500 %%%%%%%%%%%%%%%%%%%%%%%%
idx3 = 25:35;
Xaxis3 = ts_data(idx3, 2);
ts = ts_data(idx3, 8);
sw = sw_data(idx3, 8);
idx_col3 = 19:29;
axis_b3 = ts_col_data(idx_col3, 2);
ts_col3 = ts_col_data(idx_col3, 8);
sw_row3 = sw_row_data(idx_col3, 8);

subplot(2, 2, 3);
m = Xaxis3; k = 5000; n = 500;
op1D3 = (m .* (n * k)) ./ (4*(m.*k + n.*k + m.*n));
roofline1D = min(op1D3 * bandwidth, apeak);

plot(Xaxis3, roofline1D, '-', ...
     Xaxis3, ts,'-o',...
     Xaxis3, sw, '-*',...
     axis_b3, ts_col3, '-^',...
     axis_b3, sw_row3, '-x',...
     'LineWidth', LineWide);

xlim([min(Xaxis2) max(Xaxis2)]);
ylim([0 1.1*max(roofline1D)]);

axis square;
set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(c) $k=5000,\ n = 500$'},'Interpreter', 'latex',...
       'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

%%%%%%%%%% (d) rectangle 1000*10000 %%%%%%%%%%%%%%%%%%%%%%%%
idx4 = 36:46;
Xaxis4 = ts_data(idx4, 2);
ts = ts_data(idx4, 8);
sw = sw_data(idx4, 8);

subplot(2, 2, 4);
m = Xaxis4; k = 1000; n = 10000;
op1D4 = (m .* (n * k)) ./ (4*(m.*k + n.*k + m.*n));
roofline1D = min(op1D4 * bandwidth, apeak);

plot(Xaxis4, roofline1D, '-',...
     Xaxis4, ts, '-o',...
     Xaxis4, sw, '-*',...
     'LineWidth', LineWide);
 
xlim([min(Xaxis4) max(Xaxis4)]);
ylim([0 1.1*max(roofline1D)]);

axis square;
set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(d) $k=1000,\ n = 10000$'},'Interpreter', 'latex',...
       'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');