clc; clear; close all;

fsize = 15; largeFont = 20; LineWide = 2;

test_num = 4;

marker = {'-d', ':', '-o', '--', '-x', '-*', '-^', '->', '-+', '-s', '-.'};

h1=figure; set(h1, 'position', [0, 0, 800, 750]);

bandwidth = 0.013; apeak = 6.0; %% 13GB/s for pci-e 6Tflops 
data_single = csvread('single_1D_perf.csv');
%%%%%%%%%%%%%%%%%(a) square 1000 * 1000 %%%%%%%%%%%%%%%%%%%%%
data1 = csvread('single_1000.csv');
Xaxis1 = data1(:, 2);
single = data1(:, 6);
XTpin  = data1(:, 10);
XTuma  = data1(:, 12);

blasx = csvread('BLASX_1000.csv');
subplot(2,2,1);
m = Xaxis1; n = 1000;
op1D1 = (m .* n) ./ (4*(2*m + n));
roofline1D = min(op1D1 * bandwidth, apeak);

plot(Xaxis1, roofline1D, '-', ...
     Xaxis1, single,'-o',...
     Xaxis1, XTpin,'-*',...
     Xaxis1, XTuma,'-x',...
     blasx(:, 1), blasx(:, 5),'-+',...
     'LineWidth', LineWide);

xlim([min(m) max(m)]);
ylim([0 1.1* max(roofline1D)]);

h_legend = legend('roofline',...
       'Proposed',...
       'CublasXT by Pinned memory',...
       'CublasXT by UMA',...
       'BLASX',...
       'Orientation','Vertical','Location','south');

set(h_legend,'FontSize',fsize); axis square;

set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(a) $k = 1000,\ n = 1000$'}, ...
       'Interpreter', 'latex', 'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

%%%%%%%%%%%%%%%%%%% (b) square 5000*5000 %%%%%%%%%%%%%%%%%%%%%%%%
data2 = csvread('single_5000.csv');
Xaxis2 = data2(:, 2);
single = data2(:, 6);
XTpin  = data2(:, 10);
XTuma  = data2(:, 12);

blasx = csvread('BLASX_5000.csv');
subplot(2,2,2);
m = Xaxis2; n = 5000;
op1D2 = (m .* n) ./ (4*(2*m + n));
roofline1D = min(op1D2 * bandwidth, apeak);

plot(Xaxis2, roofline1D, '-', ...
     Xaxis2, single,'-o',...
     Xaxis2, XTpin,'-*',...
     Xaxis2, XTuma,'-x',...
     blasx(:, 1), blasx(:, 5),'-+',...
     'LineWidth', LineWide);
xlim([min(Xaxis2) max(Xaxis2)]);
ylim([0 1.1*max(roofline1D)]);

axis square;
set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(b) $k=5000,\ n = 5000$'}, 'Interpreter', 'latex',...
       'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);

title('');
grid on; set(gca,'GridLineStyle',':');


%%%%%%%%%% (c) rectangle 5000*500 %%%%%%%%%%%%%%%%%%%%%%%%
data3 = csvread('single_5000_500.csv');
Xaxis3 = data3(:, 2);
single = data3(:, 6);
XTpin  = data3(:, 10);
XTuma  = data3(:, 12);

blasx = csvread('BLASX_5000_500.csv');
subplot(2, 2, 3);
m = Xaxis3; k = 5000; n = 500;
op1D3 = (m .* (n * k)) ./ (4*(m.*k + n.*k + m.*n));
roofline1D = min(op1D3 * bandwidth, apeak);

plot(Xaxis3, roofline1D, '-', ...
     Xaxis3, single,'-o',...
     Xaxis3, XTpin,'-*',...
     Xaxis3, XTuma,'-x',...
     blasx(:, 1), blasx(:, 5),'-+',...
     'LineWidth', LineWide);
xlim([min(Xaxis2) max(Xaxis2)]);
ylim([0 1.1*max(roofline1D)]);

axis square;
set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(c) $k=5000,\ n = 500$'},'Interpreter', 'latex',...
       'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');

%%%%%%%%%% (d) rectangle 1000*10000 %%%%%%%%%%%%%%%%%%%%%%%%
data4 = csvread('single_1000_10000.csv');
Xaxis4 = data4(:, 2);
single = data4(:, 6);
XTpin  = data4(:, 10);
XTuma  = data4(:, 12);

blasx = csvread('BLASX_1000_10000.csv');
subplot(2, 2, 4);
m = Xaxis4; k = 1000; n = 10000;
op1D4 = (m .* (n * k)) ./ (4*(m.*k + n.*k + m.*n));
roofline1D = min(op1D4 * bandwidth, apeak);

plot(Xaxis4, roofline1D, '-',...
     Xaxis4, single, '-o',...
     Xaxis4, XTpin, '-*',...
     Xaxis4, XTuma, '-x',...
     blasx(:, 1), blasx(:, 5), '-+',...
     'LineWidth', LineWide);
xlim([min(Xaxis4) max(Xaxis4)]);
ylim([0 1.1*max(roofline1D)]);

axis square;
set(gca,'Xscale', 'log');
set(gca, 'fontsize', fsize);
%set(gca,'Xscale', 'log','Yscale','log');
xlabel({'$m$','(d) $k=1000,\ n = 10000$'},'Interpreter', 'latex',...
       'fontsize', largeFont);
ylabel('Tflop/s', 'fontsize', largeFont);
grid on; set(gca,'GridLineStyle',':');
